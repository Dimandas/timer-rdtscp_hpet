﻿#ifndef __TIMING_HH__
#define __TIMING_HH__

#include <stdint.h>
#include <time.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <bitset>
#include <array>
#include <string>
#include <intrin.h>

#define BILLION  1000000000L
#define GHZ_TO_HZ BILLION

typedef struct {
	uint8_t pid;
	uint64_t tsc;
} rdtscp_t;

extern std::ofstream logs;

class Clock
{
public:
	virtual rdtscp_t stop_time() = 0;
	virtual void stop_timeS() = 0;
	virtual double report_time() = 0;
};

class TimeStampClock : public Clock
{
public:
	TimeStampClock(uint64_t);
	rdtscp_t stop_time();
	void stop_timeS();
	double report_time();

private:
	uint64_t clock_freq;
	rdtscp_t timing_info;
	rdtscp_t rdtscp(rdtscp_t* tscp);
	void rdtscpS(rdtscp_t* tscp);
};

class Timer
{
private:
	Clock* start_clock, * end_clock;
	bool have_tocked;
	rdtscp_t checkPID;

	Timer() {};

public:
	Timer(double in_freq);
	~Timer();
	void tick(); // start timer
	void tock(); // stop timer

	void tickS(); // start timer
	void tockS(); // stop timer

	double get_time(); // get time b/t tick and tock
};

static inline bool check_rdtscp(void)
{
	int tsc_cpu;

	int cpuInfo[4] = { -1, -1, -1, -1 };

	__cpuid(cpuInfo, 0x80000001);

	tsc_cpu = cpuInfo[3];

	return (tsc_cpu & (1 << 27));
}

static inline bool check_invariantrdtscp(void)
{
	int tsc_cpu;

	int cpuInfo[4] = { -1, -1, -1, -1 };

	__cpuid(cpuInfo, 0x80000007);

	tsc_cpu = cpuInfo[3];

	return (tsc_cpu & (1 << 8));
}

static inline std::string get_vendor(void)
{
	std::vector<std::array<int, 4>> data_;
	std::string vendor_;
	int nIds_ = 0;

	std::array<int, 4> cpui;

	__cpuid(cpui.data(), 0);
	nIds_ = cpui[0];

	for (int i = 0; i <= nIds_; ++i)
	{
		__cpuidex(cpui.data(), i, 0);
		data_.push_back(cpui);
	}

	char vendor[0x20];
	memset(vendor, 0, sizeof(vendor));
	*reinterpret_cast<int*>(vendor) = data_[0][1];
	*reinterpret_cast<int*>(vendor + 4) = data_[0][3];
	*reinterpret_cast<int*>(vendor + 8) = data_[0][2];
	vendor_ = vendor;

	return vendor_;
}

static inline std::string get_brand(void)
{
	std::vector<std::array<int, 4>> extdata_;
	std::bitset<32> f_81_ECX_;
	std::bitset<32> f_81_EDX_;
	std::string brand_;
	int nExIds_ = 0;

	std::array<int, 4> cpui;

	__cpuid(cpui.data(), 0x80000000);
	nExIds_ = cpui[0];

	char brand[0x40];
	memset(brand, 0, sizeof(brand));

	for (int i = 0x80000000; i <= nExIds_; ++i)
	{
		__cpuidex(cpui.data(), i, 0);
		extdata_.push_back(cpui);
	}

	// load bitset with flags for function 0x80000001
	if (nExIds_ >= 0x80000001)
	{
		f_81_ECX_ = extdata_[1][2];
		f_81_EDX_ = extdata_[1][3];
	}

	// Interpret CPU brand string if reported
	if (nExIds_ >= 0x80000004)
	{
		memcpy(brand, extdata_[2].data(), sizeof(cpui));
		memcpy(brand + 16, extdata_[3].data(), sizeof(cpui));
		memcpy(brand + 32, extdata_[4].data(), sizeof(cpui));
		brand_ = brand;
	}

	return brand_;
}

#endif