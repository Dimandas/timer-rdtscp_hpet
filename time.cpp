﻿#include "timing.h"

#include <iostream>
#include <chrono>
#include <thread>

#ifdef _WIN32
#define _WIN32_DCOM
#include <comdef.h>
#include <Wbemidl.h>
#pragma comment(lib, "wbemuuid.lib")

std::wstring get_motherboard_manufacturer()
{
	HRESULT hres;
	std::wstring out = L"";

	hres = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hres))
	{
		return out;              // Program has failed.
	}

	hres = CoInitializeSecurity(
		NULL,
		-1,      // COM negotiates service                  
		NULL,    // Authentication services
		NULL,    // Reserved
		RPC_C_AUTHN_LEVEL_DEFAULT,    // authentication
		RPC_C_IMP_LEVEL_IMPERSONATE,  // Impersonation
		NULL,             // Authentication info 
		EOAC_NONE,        // Additional capabilities
		NULL              // Reserved
	);

	if (FAILED(hres))
	{
		CoUninitialize();
		return out;          // Program has failed.
	}

	IWbemLocator* pLoc = 0;

	hres = CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID*)&pLoc);

	if (FAILED(hres))
	{
		CoUninitialize();
		return out;       // Program has failed.
	}

	IWbemServices* pSvc = 0;

	hres = pLoc->ConnectServer(

		_bstr_t(L"ROOT\\CIMV2"), // WMI namespace
		NULL,                    // User name
		NULL,                    // User password
		0,                       // Locale
		NULL,                    // Security flags                 
		0,                       // Authority       
		0,                       // Context object
		&pSvc                    // IWbemServices proxy
	);

	if (FAILED(hres))
	{
		pLoc->Release();
		CoUninitialize();
		return out;                // Program has failed.
	}

	hres = CoSetProxyBlanket(

		pSvc,                         // the proxy to set
		RPC_C_AUTHN_WINNT,            // authentication service
		RPC_C_AUTHZ_NONE,             // authorization service
		NULL,                         // Server principal name
		RPC_C_AUTHN_LEVEL_CALL,       // authentication level
		RPC_C_IMP_LEVEL_IMPERSONATE,  // impersonation level
		NULL,                         // client identity 
		EOAC_NONE                     // proxy capabilities     
	);

	if (FAILED(hres))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return out;               // Program has failed.
	}

	IEnumWbemClassObject* pEnumerator = NULL;
	hres = pSvc->ExecQuery(
		bstr_t("WQL"),
		bstr_t("SELECT * FROM Win32_ComputerSystem"),
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		NULL,
		&pEnumerator);

	if (FAILED(hres))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return out;               // Program has failed.
	}
	else
	{
		IWbemClassObject* pclsObj;
		ULONG uReturn = 0;

		while (pEnumerator)
		{
			hres = pEnumerator->Next(WBEM_INFINITE, 1,
				&pclsObj, &uReturn);

			if (0 == uReturn)
			{
				break;
			}

			VARIANT vtProp;

			// Get the value of the Name property
			hres = pclsObj->Get(L"Manufacturer", 0, &vtProp, 0, 0);
			out = vtProp.bstrVal;
			VariantClear(&vtProp);

			pclsObj->Release();
			pclsObj = NULL;
		}

	}

	pSvc->Release();
	pLoc->Release();
	pEnumerator->Release();

	CoUninitialize();

	return out;   // Program successfully completed.
}

std::wstring get_motherboard_vendor()
{
	HRESULT hres;
	std::wstring out = L"";

	hres = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hres))
	{
		return out;              // Program has failed.
	}

	hres = CoInitializeSecurity(
		NULL,
		-1,      // COM negotiates service                  
		NULL,    // Authentication services
		NULL,    // Reserved
		RPC_C_AUTHN_LEVEL_DEFAULT,    // authentication
		RPC_C_IMP_LEVEL_IMPERSONATE,  // Impersonation
		NULL,             // Authentication info 
		EOAC_NONE,        // Additional capabilities
		NULL              // Reserved
	);

	if (FAILED(hres))
	{
		CoUninitialize();
		return out;          // Program has failed.
	}

	IWbemLocator* pLoc = 0;

	hres = CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID*)&pLoc);

	if (FAILED(hres))
	{
		CoUninitialize();
		return out;       // Program has failed.
	}

	IWbemServices* pSvc = 0;

	hres = pLoc->ConnectServer(

		_bstr_t(L"ROOT\\CIMV2"), // WMI namespace
		NULL,                    // User name
		NULL,                    // User password
		0,                       // Locale
		NULL,                    // Security flags                 
		0,                       // Authority       
		0,                       // Context object
		&pSvc                    // IWbemServices proxy
	);

	if (FAILED(hres))
	{
		pLoc->Release();
		CoUninitialize();
		return out;                // Program has failed.
	}

	hres = CoSetProxyBlanket(

		pSvc,                         // the proxy to set
		RPC_C_AUTHN_WINNT,            // authentication service
		RPC_C_AUTHZ_NONE,             // authorization service
		NULL,                         // Server principal name
		RPC_C_AUTHN_LEVEL_CALL,       // authentication level
		RPC_C_IMP_LEVEL_IMPERSONATE,  // impersonation level
		NULL,                         // client identity 
		EOAC_NONE                     // proxy capabilities     
	);

	if (FAILED(hres))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return out;               // Program has failed.
	}

	IEnumWbemClassObject* pEnumerator = NULL;
	hres = pSvc->ExecQuery(
		bstr_t("WQL"),
		bstr_t("SELECT * FROM Win32_ComputerSystem"),
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		NULL,
		&pEnumerator);

	if (FAILED(hres))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return out;               // Program has failed.
	}
	else
	{
		IWbemClassObject* pclsObj;
		ULONG uReturn = 0;

		while (pEnumerator)
		{
			hres = pEnumerator->Next(WBEM_INFINITE, 1,
				&pclsObj, &uReturn);

			if (0 == uReturn)
			{
				break;
			}

			VARIANT vtProp;

			// Get the value of the Name property
			hres = pclsObj->Get(L"Model", 0, &vtProp, 0, 0);
			out = vtProp.bstrVal;
			VariantClear(&vtProp);

			pclsObj->Release();
			pclsObj = NULL;
		}

	}

	pSvc->Release();
	pLoc->Release();
	pEnumerator->Release();

	CoUninitialize();

	return out;   // Program successfully completed.
}

#endif

std::ofstream logs;

int main(int argc, char* argv[])
{
#if defined _M_IX86
	logs.open("logs x86 " + get_brand() + ".txt");
#elif defined _M_X64
	logs.open("logs x64 " + get_brand() + ".txt");
#endif

	if (logs.is_open() == false)
	{
		return -1;
	}

#if defined _M_IX86
	std::cout << "x86 version" << std::endl;
	std::cout << std::endl;

	logs << "x86 version" << std::endl;
	logs << std::endl;
#elif defined _M_X64
	std::cout << "x64 version" << std::endl;
	std::cout << std::endl;

	logs << "x64 version" << std::endl;
	logs << std::endl;
#endif

#ifdef _WIN32
	std::wstring convert1 = get_motherboard_manufacturer();
	std::wstring convert2 = get_motherboard_vendor();

	std::string conv1(convert1.begin(), convert1.end());
	std::string conv2(convert2.begin(), convert2.end());

	std::cout << conv1 << std::endl;
	std::cout << conv2 << std::endl;
	std::cout << std::endl;

	logs << conv1 << std::endl;
	logs << conv2 << std::endl;
	logs << std::endl;
#endif

	std::cout << get_vendor() << std::endl;
	std::cout << get_brand() << std::endl;
	std::cout << std::endl;

	logs << get_vendor() << std::endl;
	logs << get_brand() << std::endl;
	logs << std::endl;

	std::cout << "RDTSCP Support:        " << (bool)check_rdtscp() << std::endl;
	std::cout << "Invariant TSC Support: " << (bool)check_invariantrdtscp() << std::endl;
	std::cout << std::endl;

	logs << "RDTSCP Support:        " << (bool)check_rdtscp() << std::endl;
	logs << "Invariant TSC Support: " << (bool)check_invariantrdtscp() << std::endl;
	logs << std::endl;

	double frequencyProcessor = 0;
	std::cout << "Enter the current processor frequency GHz (example 3.00): ";
	std::cin >> frequencyProcessor;
	std::cout << std::endl;

	logs << "Enter the current processor frequency GHz (example 3.00): " << frequencyProcessor;
	logs << std::endl;

	Timer t(frequencyProcessor);

	t.tickS();
	t.tockS();

	auto start = std::chrono::steady_clock::now();
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;

	std::cout << "Time test 0 (Resolution): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET (Resolution):        " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:               " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 0 (Resolution): " << t.get_time() << " s" << std::endl;
	logs << "HPET (Resolution):        " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:               " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 1 (Resolution): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET (Resolution):        " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:               " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 1 (Resolution): " << t.get_time() << " s" << std::endl;
	logs << "HPET (Resolution):        " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:               " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(10));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(10));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 2 (Sleep 10 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 10 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                         " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 2 (Sleep 10 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 10 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                         " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(40));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(40));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 3 (Sleep 40 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 40 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                         " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 3 (Sleep 40 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 40 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                         " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(80));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(80));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 4 (Sleep 80 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 80 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                         " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 4 (Sleep 80 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 80 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                         " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(120));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(120));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 5 (Sleep 120 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 120 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 5 (Sleep 120 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 120 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(180));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(180));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 6 (Sleep 180 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 180 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 6 (Sleep 180 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 180 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(240));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(240));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 7 (Sleep 240 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 240 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 7 (Sleep 240 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 240 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(300));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(300));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 8 (Sleep 300 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 300 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 8 (Sleep 300 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 300 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(400));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(400));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 9 (Sleep 400 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 400 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 9 (Sleep 400 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 400 nanoseconds):   " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                          " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(500));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(500));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 10 (Sleep 500 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 500 nanoseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                           " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 10 (Sleep 500 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 500 nanoseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                           " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::nanoseconds(600));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::nanoseconds(600));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 11 (Sleep 600 nanoseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 600 nanoseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                           " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 11 (Sleep 600 nanoseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 600 nanoseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                           " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 12 (Sleep 50 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 50 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                           " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 12 (Sleep 50 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 50 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                           " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 13 (Sleep 200 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 200 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                            " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 13 (Sleep 200 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 200 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                            " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 14 (Sleep 500 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 500 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                            " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 14 (Sleep 500 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 500 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                            " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 15 (Sleep 1000 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 1000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 15 (Sleep 1000 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 1000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 16 (Sleep 2000 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 2000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 16 (Sleep 2000 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 2000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 17 (Sleep 3000 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 3000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 17 (Sleep 3000 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 3000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(4000));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(4000));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 18 (Sleep 4000 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 4000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 18 (Sleep 4000 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 4000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;

	t.tick();
	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	t.tock();

	start = std::chrono::high_resolution_clock::now();
	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	end = std::chrono::high_resolution_clock::now();
	elapsed_seconds = end - start;

	std::cout << "Time test 19 (Sleep 5000 milliseconds): " << t.get_time() << " s" << std::endl;
	std::cout << "HPET Time (Sleep 5000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	std::cout << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	std::cout << std::endl;

	logs << "Time test 19 (Sleep 5000 milliseconds): " << t.get_time() << " s" << std::endl;
	logs << "HPET Time (Sleep 5000 milliseconds):    " << elapsed_seconds.count() << " s" << std::endl;
	logs << "Difference:                             " << elapsed_seconds.count() - t.get_time() << " s" << std::endl;
	logs << std::endl;


	std::cout << "Done." << std::endl;

	logs << "Done." << std::endl;
	logs.close();

	int code;
	std::cin >> code;

	return 0;
}