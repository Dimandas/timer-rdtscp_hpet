﻿#include "timing.h"
#include <intrin.h>
#include <iostream>

rdtscp_t TimeStampClock::rdtscp(rdtscp_t* tscp)
{
	unsigned pid; // processor id

	tscp->tsc = __rdtscp(&pid);
	tscp->pid = pid;

	return *tscp;
}

void TimeStampClock::rdtscpS(rdtscp_t* tscp)
{
	unsigned pid; // processor id

	tscp->tsc = __rdtscp(&pid);
	tscp->pid = pid;
}

TimeStampClock::TimeStampClock(uint64_t in_freq) : clock_freq(in_freq)
{}

inline rdtscp_t TimeStampClock::stop_time()
{
	return rdtscp(&timing_info);
}

inline void TimeStampClock::stop_timeS()
{
	rdtscpS(&timing_info);
}

double TimeStampClock::report_time()
{
	return static_cast<double>(timing_info.tsc) / clock_freq;
}

Timer::Timer(double in_freq_ghz) : have_tocked(false)
{
	start_clock = new TimeStampClock(in_freq_ghz * GHZ_TO_HZ);
	end_clock = new TimeStampClock(in_freq_ghz * GHZ_TO_HZ);
}

Timer::~Timer()
{
	delete start_clock;
	delete end_clock;
}

void Timer::tick()
{
	checkPID = start_clock->stop_time();

	std::cout << "Processor ID start: " << (unsigned long long)checkPID.pid << "       " << "TSC: " << (unsigned long long)checkPID.tsc << std::endl;

	logs << "Processor ID start: " << (unsigned long long)checkPID.pid << "       " << "TSC: " << (unsigned long long)checkPID.tsc << std::endl;

	have_tocked = false;
}

void Timer::tock()
{
	rdtscp_t PID = end_clock->stop_time();

	std::cout << "Processor ID stop:  " << (unsigned long long)PID.pid << "       " << "TSC: " << (unsigned long long)PID.tsc << std::endl;
	std::cout << "Difference:                      " << (unsigned long long)PID.tsc - (unsigned long long)checkPID.tsc << std::endl;

	logs << "Processor ID stop:  " << (unsigned long long)PID.pid << "       " << "TSC: " << (unsigned long long)PID.tsc << std::endl;
	logs << "Difference:                      " << (unsigned long long)PID.tsc - (unsigned long long)checkPID.tsc << std::endl;

	if (checkPID.pid != PID.pid)
	{
		std::cout << "------------------------------------------------" << std::endl;
		std::cout << "         WARNING Change Processor ID" << std::endl;
		std::cout << "------------------------------------------------" << std::endl;

		logs << "------------------------------------------------" << std::endl;
		logs << "         WARNING Change Processor ID" << std::endl;
		logs << "------------------------------------------------" << std::endl;
	}

	have_tocked = true;
}

void Timer::tickS()
{
	start_clock->stop_timeS();

	have_tocked = false;
}

void Timer::tockS()
{
	end_clock->stop_timeS();

	have_tocked = true;
}

double Timer::get_time()
{
	if (!have_tocked)
		return -1;

	return end_clock->report_time() - start_clock->report_time();
}